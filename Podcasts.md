## Talking Drupal

* [Talking Drupal #304](https://www.talkingdrupal.com/304) - Voice Content and Usability in Drupal - July, 29th 2021

* [Talking Drupal #305](https://www.talkingdrupal.com/305) - Project Browser - August, 5th 2021

* [Talking Drupal #307](https://www.talkingdrupal.com/307) - Mentoring, August, 19th 2021

* [Talking Drupal #318](https://www.talkingdrupal.com/318) - DDEV, October 19, 2021

* [Talking Drupal #345](https://www.talkingdrupal.com/345) - Live From DrupalCon, April 28, 2022

* [Talking Drupal #354](https://www.talkingdrupal.com/354) - OpenSource.com, June 28, 2022

* [Talking Drupal #392](https://www.talkingdrupal.com/392) - Public Speaking, March 27, 2023

* [Talking Drupal #413](https://www.talkingdrupal.com/413) - Drupal Coffee Exchange. August 28, 2023


## Talking Drupal - Skills Upgrade
A Talking Drupal mini-series following the journey of a Drupal 7 developer learning Drupal 10.

* [Talking Drupal Skills Upgrade #1](https://talkingdrupal.com/su-001) - March 06, 2024

* [Talking Drupal Skills Upgrade #2](https://talkingdrupal.com/su-002) - March 13, 2024

* [Talking Drupal Skills Upgrade #3](https://talkingdrupal.com/su-003) - March 20, 2024

* [Talking Drupal Skills Upgrade #4](https://talkingdrupal.com/su-004) - March 27, 2024

* [Talking Drupal Skills Upgrade #5](https://talkingdrupal.com/su-005) - April 03, 2024

* [Talking Drupal Skills Upgrade #6](https://talkingdrupal.com/su-006) - April 10, 2024

* [Talking Drupal Skills Upgrade #7](https://talkingdrupal.com/su-007) - April 17, 2024

* [Talking Drupal Skills Upgrade #8](https://talkingdrupal.com/su-008) - April 24, 2024

* [Talking Drupal Skills Upgrade #9](https://talkingdrupal.com/su-009) - May 01, 2024

## DrupalEasy

 * [DrupalEasy Podcast 197](https://drupaleasy.podbean.com/e/drupaleasy-podcast-197-badcamp-with-amyjune-hineline/) - BADCamp with AmyJune Hineline, October, 5th 2017

 * [DrupalEasy Podcast 219](https://www.drupaleasy.com/podcast/2019/06/drupaleasy-podcast-219-amyjune-hineline-contribution-tour-2019) - AmyJune Hineline - Contribution Tour 2019, June, 6th 2019

 * [DrupalEasy Podcast 228](https://www.drupaleasy.com/podcast/2020/04/drupaleasy-podcast-228-ofer-shaal-drupal-rector-amyjune-hineline-virtual)- Virtual Contribution Days, April 9, 2020

 * [DrupalEasy Podcast 236](https://www.drupaleasy.com/podcast/2020/08/drupaleasy-podcast-236-amyjune-hineline-virtual-drupal-events) - Virtual Drupal Events, August, 6th 2020


## Misc Podcasts

 * [Behind the Screens Episode 232](https://www.lullabot.com/podcasts/drupal-voices/232-amyjune-hineline) - Behind the Screens with AmyJune Hineline

 * [Mediacurrent: Open Waters Podcast Ep 6](https://www.pandora.com/podcast/mediacurrent-open-waters-podcast/why-companies-should-contribute-to-open-source/PE:6003253) AmyJune Hineline on Why Companies Should Contribute to Open Source, October, 24th 2019

* [Women who code - Career Nav #55](https://www.womenwhocode.com/blog/career-nav-55-how-open-source-can-help-elevate-the-careers-of-those-who-have-been-historically-marginalized/): How Open Source Can Help Elevate the Careers of Those Who Have Been Historically Marginalized, July 24, 2023

* [Torque Social Hour with AmyJune Hineline](https://www.youtube.com/watch?v=7k07uHcfOgg&t=15s) - WordFest Live, Feb 2022 -

* [Torque Social Hour with AmyJune Hineline](https://www.youtube.com/watch?v=IeIT224Dp8o) - Contributing to WordPress, September 22, 2021

* [Everyday Heroes S1E3](https://www.youtube.com/watch?v=HsBXkIURXO4) - Hospice to HOSTNAME - AmyJune Hineline's Guide to Hacking the Open Source Career Path, Feb 4, 2025
